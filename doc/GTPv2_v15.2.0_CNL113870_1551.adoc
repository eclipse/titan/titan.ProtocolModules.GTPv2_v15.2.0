---
Author: Timea Moder
Version: 1551-CNL 113 870, Rev. PB1
Date: 2018-08-03
---
= GTPv2 v15.2.0 Protocol Modules for TTCN-3 Toolset with Titan, Description
:author: Timea Moder
:revnumber: 1551-CNL 113 870, Rev. PB1
:revdate: 2018-08-03
:toc:

*Abstract*

This is the description for the GTPv2 v15.2.0 protocol module. The GTPv2 v15.2.0 protocol modules are developed for the TTCN-3 Toolset with Titan. This document should be read together with the <<_3,Product Revision Information>>.

= Functionality

The GTPv2 v15.2.0 protocol module implements the message structures of the <<_5,related protocol>> in a formalized way, using the standard specification language, TTCN-3. This allows defining of test data (templates) in the TTCN-3 language and correctly encoding/decoding messages when executing test suites using the Titan TTCN-3 test environment.

The GTPv2 v15.2.0 protocol module uses <<_4,Titan’s RAW encoding attributes>> and hence is usable with the Titan test toolset only.

== Implemented protocols

This set of protocol modules implements protocol messages and constants of the GTPv2 v15.2.0 protocol as described <<_5,here>>.

=== Modified and non-implemented Protocol Elements

None.

=== Ericsson-specific changes

The Ericsson Private Extensions are defined <<_6,here>>.

== Backward incompatibilities

None.

== System Requirements

Protocol modules are a set of TTCN-3 source code files that can be used as part of TTCN-3 test suites only. Hence, protocol modules alone do not put specific requirements on the system used. However, in order to compile and execute a TTCN-3 test suite using the set of protocol modules the following system requirements must be satisfied:

Titan TTCN-3 Test Executor version CRL 113 200/6 R3A (6.3.pl0) or higher installed. For Installation Guide, see <<_2,here>>.

NOTE: This version of the test port is not compatible with Titan releases earlier than CRL 113 200/6 R3A.

= Usage

== Installation

The set of protocol modules can be used in developing TTCN-3 test suites using any text editor; however, to make the work more efficient, a TTCN-3-enabled text editor is recommended (for example nedit, xemacs). Since the GTPv2 v15.2.0 protocol is used as a part of a TTCN-3 test suite, this requires TTCN-3 Test Executor to be installed before the module can be compiled and executed together with other parts of the test suite. For more details on the installation of TTCN-3 Test Executor, see the relevant section of <<_2,User Guide for TITAN TTCN-3 Test Executor>>.

== Configuration

None.

== Examples

None.

= Interface description

== Top Level PDU

The top level PDUs are the TTCN-3 records PDU_GTPCv2.

[[encoding-decoding-and-other-related-functions]]
== Encoding/decoding and other related functions

This product also contains encoding/decoding functions, which assure correct encoding of messages when sent from Titan and correct decoding of messages when received by Titan.

=== Implemented encoding and decoding functions
[options="header"]
|===
|Name |Type of formal parameters |Type of return value
|`enc_PDU_GTPCv2` |in PDU_GTPCv2 |octetstring
|`dec_PDU_GTPCv2` |in octetstring |PDU_GTPCv2
|`dec_PDU_GTPCv2_backtrack`| in octetstring, out PDU_GTPCv2| integer
|`dec_PDU_GTPCv2_fast`| in octetstring, out PDU_GTPCv2 |integer
|===

= Terminology

== Abbreviations

PDU:: Protocol Data Unit

GTPv2:: GPRS Tunnelling Protocol version 2

TTCN-3:: Testing and Test Control Notation version 3

= References

1. [[_1]]ETSI ES 201 873-1 v4.5.1 (2013-04) +
The Testing and Test Control Notation version 3. Part 1: Core Language

2. [[_2]]link:https://github.com/eclipse/titan.core/blob/master/usrguide/userguide[1/ 198 17-CRL 113 200/6] Uen +
User Guide for TITAN TTCN-3 Test Executor

3. [[_3]]109 21-CNL 113 870-1 +
GTPv2 v15.2.0 Protocol Modules for TTCN-3 Toolset with Titan Product Revision Information

4. [[_4]]link:https://github.com/eclipse/titan.core/blob/master/usrguide/referenceguide[2/198 17-CRL 113 200/6] Uen +
Programmer’s Technical Reference for Titan TTCN–3 Test Executor

5. [[_5]]3GPP TS 29.274 v15.2.0 (2017-12) +
3rd Generation Partnership Project; Technical Specification Group Core Network and Terminals; 3GPP Evolved Packet System (EPS);Evolved General Packet Radio Service (GPRS) Tunnelling Protocol for Control plane (GTPv2-C);Stage 3 (Release 15)

6. [[_6]]12/155 19-AXB 250 05 Uen Rev PK4 +
SGSN-MME Private Extensions under IANA Enterprise number 10923
